package com.example.simple;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Component
public class FilterFactory extends AbstractGatewayFilterFactory<FilterFactory.ArgsFiltersDTO> {

	private static final String FILTER_OAUTH_NAME = "OauthSecurity";
	public FilterFactory() {
		super(ArgsFiltersDTO.class);
	}

	@Override
	public GatewayFilter apply(ArgsFiltersDTO config) {
		return (exchange, chain) -> {
			ServerHttpRequest request = exchange.getRequest().mutate()
					.headers(httpHeaders -> httpHeaders.set("iss", config.getScope())).build();
			return chain.filter(exchange.mutate().request(request).build());
		};
	}

	@Override
	public String name() {
		return FILTER_OAUTH_NAME;
	}

	@Setter
	@Getter
	public static class ArgsFiltersDTO {
		private String scope;
	}

}