package com.example.simple;

import reactor.core.publisher.Mono;

public interface IConsumoInstrospectService {
	Mono<ResponseIntrospectDTO> callInstrospect(String Jwt, String AccessToken, String Scope, String tipoAccessToken);
}
