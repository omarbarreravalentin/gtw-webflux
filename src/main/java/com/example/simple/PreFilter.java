package com.example.simple;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class PreFilter implements GlobalFilter {

	@Autowired
	private IConsumoInstrospectService consumoIntrospect;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

		Mono<ResponseIntrospectDTO> rep = consumoIntrospect
				.callInstrospect("firmaCnLlavePrivada", "getAccessToken()","getScope()", "IN");
		//String token=rep.block().getJwt();
		
		String token="x";
		
		log.info("token "+token);
		String authHeader = "Bearer " +token ;
	
		ServerHttpRequest mutatedRequest = exchange.getRequest().mutate().header(HttpHeaders.AUTHORIZATION, authHeader)
				.build();
		exchange = exchange.mutate().request(mutatedRequest).build();
		return chain.filter(exchange);
	}
}