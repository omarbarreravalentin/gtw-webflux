package com.example.simple;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class ConsumoInstrospectService implements IConsumoInstrospectService {

    @Value("${endPoint}")
    private String endPoint;

    @Bean
    public WebClient getWebClient() {
        return WebClient.builder().build();
    }

    @Override
    public Mono<ResponseIntrospectDTO> callInstrospect(String Jwt, String AccessToken, String Scope, String tipoAccessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content_Type", "application/x-www-form-urlencoded");
        headers.add(AUTHORIZATION, "Bearer " + Jwt);
        headers.add("need-jwt", "yes");

        LinkedMultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("token", AccessToken);
        body.add("token_type_hint", "access_token");
        body.add("iss", Scope);

        Mono<ResponseIntrospectDTO> respService= getWebClient().post()
                .uri(endPoint)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData(body))
                .headers(x -> x.addAll(headers))
                .acceptCharset(StandardCharsets.UTF_8)
                .retrieve()
                .bodyToMono(ResponseIntrospectDTO.class);
        return respService;
        
    }

}
