package com.example.simple;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponseIntrospectDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String jwt;
	private String callError;
}
